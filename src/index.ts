'use strict'

import { ipcRenderer } from 'electron'
// create add renderer window button
document.getElementById('tellRendererFromMainBtn').addEventListener('click', () => {
    ipcRenderer.send('renderer-window')
})

// on receive message
ipcRenderer.on('recieve-message', (event: Event, messages: String[]) => {
    // get the messages ul
    const rendererMessageList = document.getElementById('rendererMessageList')

    let html = ''
    messages.forEach(message => {
        html = html + `<li>${message}</li>`
    })
  
    if (typeof html !== 'undefined' || html !== '') {
        // set value in html tag
        rendererMessageList.innerHTML = html
    }

})

// movies section
document.getElementById('openMovieWindowBtn').addEventListener('click', () => {
    ipcRenderer.send('movie-window')
})
