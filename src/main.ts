import { app, BrowserWindow, ipcMain } from 'electron'
import * as path from 'path'

let mainWindow: Electron.BrowserWindow
let messages: String[] = []

function createWindow() {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    height: 600,
    width: 800,
  })

  // and load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, '../index.html'))

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  let rendererWindow: Electron.BrowserWindow
  // create renderer window
  ipcMain.on('renderer-window', () => {
    // if rendererWindow does not already exist
    if (!rendererWindow) {
      // create a new renderer window
      rendererWindow = new BrowserWindow({
        // file: path.join(__dirname, 'renderer.html'),
        width: 600,
        height: 400,
        // close with the main window
        parent: mainWindow,
        webPreferences: {
          nodeIntegration: true
        }
      })     
      // and load the renderer.html of the app.
      // rendererWindow.loadFile(path.join(__dirname, '../src/renderer/renderer.html'))
      rendererWindow.loadFile(path.join(__dirname, '../renderer.html'))

      rendererWindow.once('ready-to-show', () => {
        rendererWindow.show()
      })

      // cleanup
      rendererWindow.on('closed', () => {
        rendererWindow = null
      })
    }
  })

  // create movie window
  let movieWindow: Electron.BrowserWindow
  ipcMain.on('movie-window', () => {
    // if movieWindow does not already exist
    if (!movieWindow) {
      // create a new renderer window
      movieWindow = new BrowserWindow({
        // file: path.join(__dirname, 'renderer.html'),
        width: 600,
        height: 400,
        // close with the main window
        parent: mainWindow,
        webPreferences: {
          nodeIntegration: true
        }
      })     
      // and load the movies.html of the app.
      movieWindow.loadFile(path.join(__dirname, '../movies.html'))

      movieWindow.once('ready-to-show', () => {
        movieWindow.show()
      })

      // cleanup
      movieWindow.on('closed', () => {
        movieWindow = null
      })
    }
  })

  // add message from renderer window
  ipcMain.on('tell-message-to-main', (event:  Event, newMessage: String) => {
    messages = [ ...messages, newMessage ]
    // send ไปที่ตัว index renderer
    mainWindow.webContents.send('recieve-message', messages)
    // คำถาม webContents.send ต่างจาก send อย่างไร เพราะผลลัพธ์มันออกมาเหมือนกัน
    // mainWindow.webContent.send('recieve-message', messages) 

    // send ไปที่ตัว renderer renderer
    rendererWindow.webContents.send('recieve-message', messages)
  })

  // Emitted when the window is closed.
  mainWindow.on("closed", () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", createWindow)

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    app.quit()
  }
})

app.on("activate", () => {
  // On OS X it"s common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.
