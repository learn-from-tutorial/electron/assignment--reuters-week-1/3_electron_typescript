'use strict'

import * as electron from 'electron'
import axios from 'axios'

interface Movie {
    id: String,
    title: String,
    images: {
        poster: String,
        background: String
    },
    genres: String[]
} 

let movies: Movie[] = []
axios.defaults.headers.post['Content-Type'] = 'application/json'

const fetchMovies = () => {
    axios.get('http://localhost:8010/api/movies')
        .then (res => {
            movies = res.data
            console.log("movies: ", movies)
            createMoviesHtml()
        })
        .catch (err => {
            console.error(err.response)
        })
}

const createMoviesHtml = () => {
    const moviesList = document.getElementById('moviesList')

        let html = ''
        movies.forEach(movie => {
            html = html + ` 
            <div class="column is-one-quarter has-margin-bottom-3">
                    <img src="${movie.images.poster}">
                    <strong>
                        ${movie.title}
                    </strong>
            </div>`
        })
      
        if (typeof html !== 'undefined' || html !== '') {
            // set value in html tag
            moviesList.innerHTML = html
        }
}


document.addEventListener('DOMContentLoaded', (event: Event) => {
    // call movies api
    fetchMovies()
})